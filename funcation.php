<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan 5</title>
</head>
<body>
  <h2>Hasil Nilai</h2>
    <?php
      echo "Jika nilai 86 : ".tentukan_nilai(86) . "<br/>";
      echo "Jika nilai 73 : ".tentukan_nilai(79) . "<br/>";
      echo "Jika nilai 62 : ".tentukan_nilai(68) . "<br/>";
      echo "Jika nilai 38 : ".tentukan_nilai(40) . "<br/>";

      function tentukan_nilai($nilai)
      {
          if($nilai > 85 && $nilai < 100)
          {
              return "Sangat Baik";
          }
          else if($nilai > 70 && $nilai < 85)
          {
            return "Baik";
          }
          else if($nilai > 60 && $nilai < 70)
          {
            return "Cukup";
          }
          else
          {
            return "Kurang";
          }
      }
    ?>
</body>
</html>
